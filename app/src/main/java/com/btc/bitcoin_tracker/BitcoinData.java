package com.btc.bitcoin_tracker;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class BitcoinData {
    private String price;
    private String currencyCrypto;
    private String currencyFiat;
    private String fiatSymbol;

    //CONSTRUCTORS

    public BitcoinData(){
        //Build empty
        currencyCrypto = null;
        currencyFiat = null;
        price = null;
        fiatSymbol = null;
    }

    public BitcoinData(JSONObject json, String currency){
        //Build from json
        try {
            JSONObject data = json.getJSONObject(currency);
            currencyCrypto = "BTC";
            currencyFiat = currency;
            price = data.getString("last");
            fiatSymbol = data.getString("symbol");
            Log.d("BTC_TRC", "Data received. Price: " + price);
        } catch (JSONException e) {
            Log.d("BTC_TRC", "Json exception has occurred");
        }
    }

    //GETTERS

    public String getFiatSymbol() {
        return fiatSymbol;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrencyCrypto() {
        return currencyCrypto;
    }

    public String getCurrencyFiat() {
        return currencyFiat;
    }
}
