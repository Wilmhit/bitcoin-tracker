package com.btc.bitcoin_tracker;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;

import static android.R.layout.simple_spinner_dropdown_item;
import static android.R.layout.simple_spinner_item;

public class MainActivity extends AppCompatActivity {

    final String DEFAULT_CURRENCY = "PLN";
    final String BTC_URL = "https://blockchain.info/ticker";
    final int UPDATE_DELAY = 5000; //ten seconds

    Spinner currencySpinner;
    BitcoinData currentData;
    TextView priceTV;
    String selectedCurrency;
    Runnable periodicTask;
    Handler periodicTaskHandler;
    Boolean autoUpdateActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set member variables
        currencySpinner = findViewById(R.id.spinner);
        priceTV = findViewById(R.id.PriceTV);
        selectedCurrency = DEFAULT_CURRENCY;
        currentData = new BitcoinData();
        periodicTaskHandler = new Handler();
        autoUpdateActive = true;
        periodicTask = new Runnable() {
            @Override
            public void run() {
                Log.d("BTC_TRC","Autoupdating...");
                updateData();
                Log.d("BTC_TRC", "Should autoupdate:" + autoUpdateActive.toString());
                if (autoUpdateActive) {
                    periodicTaskHandler.postDelayed(periodicTask, UPDATE_DELAY);
                    Log.d("BTC_TRC", "Next autoupdate scheduled");
                }
                Log.d("BTC_TRC","Autoupdate finished");
            }
        };

        //Populate spinner
        ArrayAdapter<CharSequence> spinnerContents = ArrayAdapter.createFromResource(
                this,
                R.array.spinner_contets,
                simple_spinner_item
                );
        spinnerContents.setDropDownViewResource(simple_spinner_dropdown_item);
        currencySpinner.setAdapter(spinnerContents);

        //set listener on currency spinner
        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCurrency = (String) parent.getItemAtPosition(position);
                currentData = new BitcoinData(); // "Fetching data" stored in currentData
                updatePrice(); // Displays "Fetching data"
                updateData(); // Loads requested currentData
                Log.d("BTC_TRC", "Currency updated");
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCurrency = (String) parent.getItemAtPosition(0);
                updatePrice();
            }
        });

        Log.d("BTC_TRC", "OnCreate finished");
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopCycle();
        Log.d("BTC_TRC", "OnPause finished");
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCycle();
        Log.d("BTC_TRC", "OnResume finished");
    }

    //Updates price seen on the screen to price stored in currentData
    private void updatePrice(){
        Log.d("BTC_TRC", "Updating UI...");
        if (currentData.getPrice() == null) {
            priceTV.setText(R.string.price_not_loaded);
        } else {
            priceTV.setText(currentData.getPrice() + currentData.getFiatSymbol());
        }
    }

    //Updates currentData to online value
    private void updateData(){
        Log.d("BTC_TRC", "Updating data...");
        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.get(BTC_URL, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                currentData = new BitcoinData(response, selectedCurrency);
                updatePrice();
            }
        });
    }

    //starts autorefresh
    private void startCycle() {
        autoUpdateActive = true;
        periodicTaskHandler.postDelayed(periodicTask, 2000);
    }

    //stops autorefresh
    private void stopCycle() {
        autoUpdateActive = false;
    }
}
;